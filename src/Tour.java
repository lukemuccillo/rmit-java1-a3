public class Tour
{
   private String tourID;
   private String tourDescription;
   private double tourFee;
   private int totalBookings;

   public Tour(String tourID, 
      String tourDescription, double tourFee)
   {
      this.tourID = tourID;
      this.tourDescription = tourDescription;
      this.tourFee = tourFee;
   }

   public Tour(Tour tour)
   {
      this(tour.getTourID(),
           tour.getTourDescription(),
           tour.getTourFee());
   }

   public String getTourID()
   {
      return tourID;
   }

   public String getTourDescription()
   {
      return tourDescription;
   }

   public double getTourFee()
   {
      return tourFee;
   }

   public int getTotalBookings()
   {
      return totalBookings;
   }

   public double addBookings(int numberOfTourists) throws TourException
   {
      if (numberOfTourists <= 0)
      {
         throw new TourException("Number of tourists must be greater than zero");
      }
      else
      {
         totalBookings += numberOfTourists;

         double paymentDue = this.calculateBookingFees(numberOfTourists);

         return paymentDue;
      }
   }

   public double cancelBookings(int numberOfTourists) throws TourException
   {
      if (numberOfTourists <= 0)
      {
         throw new TourException("Number of tourists must be greater than zero.");
      }

      if (numberOfTourists > totalBookings)
      {
         String formatBase = "Attempted to cancel more bookings than are current,";
         formatBase += " the tour currently has %s bookings.";

         String errorMessage =String.format(formatBase, this.totalBookings);
         throw new TourException(errorMessage);
      }

      totalBookings -= numberOfTourists;

      double refundedFees = this.calculateBookingFees(numberOfTourists);

      return refundedFees;
   }

   private double calculateBookingFees(int numberOfTourists)
   {
      return numberOfTourists * tourFee;
   }

   public void displayTourDetails()
   {
      System.out.printf("%s %s\n", "Tour ID:", tourID);
      System.out.printf("%s %s\n", "Description:", tourDescription);
      System.out.printf("%s $%.2f\n", "Tour Fee:", tourFee);
      System.out.printf("%s %s\n", "Total Bookings:", totalBookings);

      System.out.printf("%s $%.2f\n\n", "Total Booking Fees Received:",
                        this.calculateBookingFees(totalBookings));
   }

   public String getFileWritableString(String delimiter)
   {
      String output = String.format("%s%s", this.tourID, delimiter);
      output += String.format("%s%s", this.tourDescription, delimiter);
      output += String.format("%s%s", this.totalBookings, delimiter);
      output += String.format("%.2f%s", this.tourFee, delimiter);
      return output;
   }
}
