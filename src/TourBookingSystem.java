import java.util.ArrayList;
import java.util.Scanner;

public class TourBookingSystem
{
   private static ArrayList<Tour> tours = new ArrayList<Tour>();

   private static final String exportFilePath = "tours.txt";

   private static final String fileDelimiter = "%%";

   private static final Scanner sc = new Scanner(System.in);

   private static void flushScanner(Scanner sc)
   {
      if (sc.hasNextLine())
      {
         sc.nextLine();
      }
   }

   private static boolean tourExists(String tourID)
   {
      return getTourIndexByID(tourID) != -1;
   }

   private static int getTourIndexByID(String tourID)
   {
      for (int i = 0; i < tours.size(); i++)
      {
         if (tours.get(i).getTourID().equals(tourID))
         {
            return i;
         }
      }

      return -1;
   }

   private static void addNewTour(boolean isGuided)
   {
      Tour newTour = null;

      System.out.println("Add New Tour");
      System.out.println("----------------");

      String tourDescription = "";

      while (tourDescription.equals(""))
      {
         System.out.print("Enter an Description for this new Tour: ");
         tourDescription = sc.nextLine();

         if (tourDescription.equals(""))
         {
            System.out.println("Error: Description cannot be empty");
         }
      }

      // trimEnd
      tourDescription = tourDescription.replaceAll(" +$", "");

      System.out.print("Enter a Cost for this new Tour: ");

      while (!sc.hasNextDouble())
      {
         System.out.println("Error: cost must be numeric");
         sc.nextLine();
         System.out.print("Enter a Cost for this new Tour: ");
      }

      double tourCost = sc.nextDouble();

      flushScanner(sc);

      boolean isValidTourID = false;
      String tourID = null;

      while (!isValidTourID)
      {
         System.out.print("Enter an ID for this new Tour: ");

         tourID = sc.nextLine().toUpperCase();
         tourID = tourID.replaceAll(" +$", "");

         isValidTourID = !tourExists(tourID) && !tourID.equals("");

         if (!isValidTourID)
         {
            if (tourID.equals(""))
            {
               System.out.println("Error: TourID cannot be empty.");
            }
            else
            {
               System.out.println("The Tour ID you have chosen already exists");
               System.out.println("Please try again.");
            }

         }
      }

      if (isGuided == true)
      {
         System.out.print("Enter the tour date: ");
         String tourDate = sc.nextLine();
         tourDate = tourDate.replaceAll(" +$", "");

         int maxGroupSize = 0;

         // Nested while look. No choice really...
         while (maxGroupSize <= 0)
         {
            System.out.print("Enter the maximum number of tourists: ");
            while (!sc.hasNextInt())
            {
               System.out.print("Error: number of tourists must be numeric\n");
               sc.nextLine();
               System.out.print("Enter the maximum number of tourists: ");
            }

            maxGroupSize = sc.nextInt();
            flushScanner(sc);

            if (maxGroupSize <= 0)
            {
               System.out
                        .print("Error: maximum number of tourists must be 1 or greater\n");
            }
         }

         System.out.print("Enter the name of the tour guide: ");
         String tourLeader = sc.nextLine();
         // trimEnd
         tourLeader = tourLeader.replaceAll(" +$", "");

         newTour = new GuidedTour(
                                  tourID, tourDescription, tourCost,
                                  tourDate, tourLeader, maxGroupSize);
      }
      else
      {
         newTour = new Tour(tourID, tourDescription, tourCost);
      }

      tours.add(newTour);
      System.out.print("\nNew tour Created:\n");
      newTour.displayTourDetails();
   }

   private static void addTourBooking()
   {
      System.out.println("Add New Tour Booking");
      System.out.println("----------------");
      System.out.print("Enter tour ID: ");
      String tourID = sc.nextLine().toUpperCase();

      int tourIndex = getTourIndexByID(tourID);

      if (tourIndex == -1)
      {
         System.out
                  .print("Unable to locate a booking with the specified ID.\n\n");
         return;
      }

      System.out.print("Enter the numer of tourists: ");
      int numberOfTourists = sc.nextInt();
      flushScanner(sc);

      try
      {
         double paymentDue =
                  tours.get(tourIndex).addBookings(numberOfTourists);
         System.out.print("Tour booking completed successfully.\n");
         System.out.format("Total payment due: $%.2f (%s x $%.2f)%n%n"
                           , paymentDue, numberOfTourists, tours.get(tourIndex)
                                    .getTourFee());
      }
      catch (TourException e)
      {
         System.out.format("Error: %s%n%n", e.getMessage());
      }
   }

   private static void cancelTourBooking()
   {
      System.out.println("Cancel Tour Booking");
      System.out.println("----------------");
      System.out.print("Enter tour ID: ");
      String tourID = sc.nextLine().toUpperCase();

      int tourIndex = getTourIndexByID(tourID);

      if (tourIndex == -1)
      {
         System.out
                  .print("Unable to locate a booking with the specified ID.\n\n");
         return;
      }

      System.out.print("Enter the numer of tourists: ");
      int numberOfTourists = sc.nextInt();
      flushScanner(sc);

      try
      {
         double refund = tours.get(tourIndex).cancelBookings(numberOfTourists);
         System.out.print("Tour cancellation completed successfully.\n");
         System.out.format("Total refund due: $%.2f (%s x $%.2f)%n%n"
                           , refund, numberOfTourists, tours.get(tourIndex)
                                    .getTourFee());
      }
      catch (TourException e)
      {
         System.out.format("Error: %s%n%n", e.getMessage());
      }

   }

   private static void updateGuidedTourGroupSize()
   {
      System.out.println("Update Guided Tour Group Size");
      System.out.println("----------------");
      System.out.print("Enter tour ID: ");
      String tourID = sc.nextLine().toUpperCase();

      // Not using tourExists() here.
      // ID required to modify the tour.
      int tourIndex = getTourIndexByID(tourID);

      if (tourIndex == -1)
      {
         System.out
                  .print("Unable to locate a booking with the specified ID.\n\n");
         return;
      }

      if (!(tours.get(tourIndex) instanceof GuidedTour))
      {
         System.out
                  .print("Error: The specified tour is not a guided tour.\n\n");
         return;
      }

      System.out.print("Enter the new tour capacity: ");
      int newGroupSize = sc.nextInt();
      flushScanner(sc);

      GuidedTour currentTour = (GuidedTour) tours.get(tourIndex);
      try
      {
         currentTour.updateMaxGroupSize(newGroupSize);
         System.out.print("Tour capacity updated successfully.\n\n");
         System.out.println();
      }
      catch (Exception e)
      {
         System.out.format("Error: %s%n%n", e.getMessage());
      }

   }

   private static void displayTourSummaryAll()
   {
      System.out.print("Summary of all tours\n");
      System.out.println("----------------");

      for (int i = 0; i < tours.size(); i++)
      {
         tours.get(i).displayTourDetails();
         System.out.println();
      }
   }

   private static void writeToursToFile(TourBookingSystemFileHandler fileHandler)
   {
      System.out.println("Writing Tours to disk..");
      boolean exportResult =
               fileHandler.exportTours(tours, exportFilePath);

      if (exportResult == true)
      {
         System.out.format("Successfully wrote %s tours to file%n",
                           tours.size());
      }
      else
      {
         System.out.println("Error: Failed to write tours to file.");
      }
   }

   public static void main(String[] args)
   {
      TourBookingSystemFileHandler fileHandler =
               new TourBookingSystemFileHandler(fileDelimiter, 4, 7);

      try
      {
         System.out.format("Importing tours from file %s%n", exportFilePath);
         tours = fileHandler.importTours(exportFilePath);
         System.out.format("%s Tours imported successfully%n%n", tours.size());
      }
      catch (Exception e)
      {
         System.out.format("Import Error: %s%n%n", e.getMessage());
      }

      char selection = '\0';

      while (selection != 'X')
      {
         System.out.print("Tour System Menu\n");
         System.out.println("----------------");
         System.out.println("A - Add New Tour");
         System.out.println("B - Display Tour Summary");
         System.out.println("C - Add Tour Booking(s)");
         System.out.println("D - Cancel Tour Booking(s)");
         System.out.println("E - Add New Guided Tour");
         System.out.println("F - Update Guided Tour Group Size");
         System.out.println("X - Exit Program");
         System.out.println();

         System.out.print("Enter your selection: ");
         String input = sc.nextLine();

         if (input.length() != 1)
         {
            System.out
                     .println("Error - selection must be a single character!\n\n");
            continue;
         }

         System.out.print("\n\n");

         selection = Character.toUpperCase(input.charAt(0));

         if (selection == 'A')
         {
            addNewTour(false);
         }
         else if (selection == 'B')
         {
            displayTourSummaryAll();
         }
         else if (selection == 'C')
         {
            addTourBooking();
         }
         else if (selection == 'D')
         {
            cancelTourBooking();
         }
         else if (selection == 'E')
         {
            addNewTour(true);
         }
         else if (selection == 'F')
         {
            updateGuidedTourGroupSize();
         }
         else if (selection == 'X')
         {
            writeToursToFile(fileHandler);
            System.out.println("Tour system shutting down � goodbye!");
         }
         else
         {
            System.out.println("Error - invalid selection!\n\n");
         }
      }
   }
}
