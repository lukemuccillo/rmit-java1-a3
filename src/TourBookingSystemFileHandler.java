import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class TourBookingSystemFileHandler
{
   public TourBookingSystemFileHandler(String objectSeparator,
                                       int numberOfTourFields,
                                       int numberOfGuidedTourFields)
   {
      this.objectSeparator = objectSeparator;
      this.numberOfTourFields = numberOfTourFields;
      this.numberOfGuidedTourFields = numberOfGuidedTourFields;
   }

   private String objectSeparator;

   private int numberOfTourFields;

   private int numberOfGuidedTourFields;

   public boolean exportTours(ArrayList<Tour> tours, String filePath)
   {
      PrintWriter writer;

      try
      {
         writer = new PrintWriter(filePath, "UTF-8");

         for (int i = 0; i < tours.size(); i++)
         {
            writer.println(tours.get(i)
                     .getFileWritableString(this.objectSeparator));
         }

         writer.close();

         return true;
      }
      catch (Exception e)
      {
         return false;
      }
   }

   public ArrayList<Tour> importTours(String filePath)
            throws Exception
   {
      ArrayList<Tour> tours = new ArrayList<Tour>();

      try
      {
         Scanner fsc = new Scanner(new File(filePath));

         while (fsc.hasNextLine())
         {
            String line = fsc.nextLine();

            String[] tokens = line.split(this.objectSeparator);

            if (tokens.length != this.numberOfTourFields
                && tokens.length != this.numberOfGuidedTourFields)
            {
               throw new Exception("Unexpected Field Formatting");
            }

            String tourID = tokens[0];
            String tourDescription = tokens[1];
            double tourFee = Double.parseDouble(tokens[3]);
            int totalBookings = Integer.parseInt(tokens[2]);

            Tour tour = new Tour(tourID, tourDescription, tourFee);

            if (tokens.length == this.numberOfGuidedTourFields)
            {
               int groupSize = Integer.parseInt(tokens[4]);
               String tourDate = tokens[5];
               String tourLeader = tokens[6];

               tour = new GuidedTour(tour, tourDate, tourLeader, groupSize);
            }

            if (totalBookings > 0)
            {
               tour.addBookings(totalBookings);
            }

            tours.add(tour);
         }

         fsc.close();

      }
      catch (FileNotFoundException e)
      {
         throw new Exception("File not found");
      }
      catch (TourException e)
      {
         throw new Exception(e.getMessage());
      }

      return tours;
   }
}
