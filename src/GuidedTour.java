public class GuidedTour extends Tour
{

   public GuidedTour(String tourID, String tourDescription,
                     double tourFee, String tourDate,
                     String tourLeader, int groupSize)
   {
      super(tourID, tourDescription, tourFee);

      this.tourDate = tourDate;
      this.groupSize = groupSize;
      this.tourLeader = tourLeader;
   }

   public GuidedTour(Tour tour, String tourDate,
                     String tourLeader, int groupSize)
   {
      super(tour);

      this.tourDate = tourDate;
      this.groupSize = groupSize;
      this.tourLeader = tourLeader;
   }

   private String tourDate;
   private String tourLeader;
   private int groupSize;

   public String getTourDate()
   {
      return this.tourDate;
   }

   public int getGroupSize()
   {
      return this.groupSize;
   }

   public int getPlacesRemaining()
   {
      return this.groupSize - this.getTotalBookings();
   }

   public String getTourLeader()
   {
      return this.tourLeader;
   }

   public void updateMaxGroupSize(int groupSize) throws TourException
   {
      if (groupSize <= 0)
      {
         throw new TourException("The group size must be greater than 0");
      }

      if (groupSize < this.getTotalBookings())
      {
         int requiredCancellations = (groupSize - this.getTotalBookings()) * -1;
         String message =
                  String.format("You must cancel at least %s bookings in order to reduce the group size to %s",
                                requiredCancellations, groupSize);
         throw new TourException(message);
      }

      this.groupSize = groupSize;
   }

   public double addBookings(int numberOfTourists) throws TourException
   {
      int newBookingCount = super.getTotalBookings() + numberOfTourists;

      if (newBookingCount > this.groupSize)
      {
         String errorMessage =
                  String.format("There are only %s places remaining.",
                                this.getPlacesRemaining());
         throw new TourException(errorMessage);
      }

      return super.addBookings(numberOfTourists);
   }

   public void displayTourDetails()
   {
      System.out.printf("%s %s\n", "Tour ID:", this.getTourID());
      System.out.printf("%s %s\n", "Description:", this.getTourDescription());
      System.out.printf("%s %s\n", "Tour Date:", this.tourDate);
      System.out.printf("%s %s\n", "Tour Leader:", this.tourLeader);
      System.out.printf("%s %s\n", "Tour capacity (max number of tourists):",
                        this.groupSize);
      System.out.printf("%s $%.2f\n", "Tour Fee:", this.getTourFee());
      System.out.printf("%s %s\n", "Total Bookings:", this.getTotalBookings());
      System.out.printf("%s %s\n", "Places Left:", this.getPlacesRemaining());
      System.out.printf("%s $%.2f\n\n", "Total Booking Fees Received:",
                        this.getTotalBookings() * this.getTourFee());
   }

   public String getFileWritableString(String delimiter)
   {
      String output = super.getFileWritableString(delimiter);
      output += String.format("%s%s", this.groupSize, delimiter);
      output += String.format("%s%s", this.tourDate, delimiter);
      output += String.format("%s%s", this.tourLeader, delimiter);
      return output;
   }
}
